public class FindGrade {

	public static void main(String[] args){
		int grade = Integer.parseInt(args[0]);
	
		if(grade > 100 || grade < 0){
			System.out.println("Your grade is not between 100 and 0");
		} else if(grade <= 100 && grade >= 90){
			System.out.println("A");
		} else if(grade < 90 && grade >= 80){
			System.out.println("B");
		} else if(grade < 80 && grade >= 70){
			System.out.println("C");
		} else if(grade < 70 && grade >= 60){
			System.out.println("D");	
		} else{
			System.out.println("F");
		}
	}
}
